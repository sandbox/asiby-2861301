<?php

/**
 * @file
 * TODO: Enter file description here.
 */

/**
 * Form builder.
 */
function custom_node_add_settings_form($form, &$form_state) {
  $form['custom_node_add_variable_foo'] = array(
    '#type' => 'textfield',
    '#title' => t('Foo'),
    '#default_value' => variable_get('custom_node_add_variable_foo', 42),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
